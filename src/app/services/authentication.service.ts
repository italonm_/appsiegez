import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { Usuario } from '../models/usuario';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { EncryptDecryptService } from './EcryptDecryptService';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

  private currentUserSubject: BehaviorSubject<Usuario>;
  public currentUser: Observable<Usuario>;

  constructor(
    private http: HttpClient,
    private crypt: EncryptDecryptService,
    private router: Router,
  ) {
    this.currentUserSubject = new BehaviorSubject<any>(localStorage.getItem('currentUser'));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): Usuario {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    return this.http.post<any>(`${environment.url_basic}${environment._user.auth}`, {username: username, password: password })
      .pipe(map(user => {
        console.log('auth serv, res', user)
        localStorage.setItem('currentUser', user.token);
        const encryptd = this.crypt.encrypt(username);
        localStorage.setItem('userName', encryptd);
        this.currentUserSubject.next(user);
        return user;
      }));
  }

  logout() {
    localStorage.clear();
    this.currentUserSubject.next(null);
    this.router.navigate(['login']);
  }
}
