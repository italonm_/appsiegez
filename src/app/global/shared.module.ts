import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { 
	IgxButtonModule, IgxIconModule, IgxNavigationDrawerModule, IgxRippleModule, IgxToggleModule, IgxInputGroupModule, IgxCardModule, IgxDividerModule, IgxExpansionPanelModule, IgxListModule, IgxSliderModule, IgxChipsModule, IgxDialogModule, IgxTooltipModule, IgxTabsModule, IgxCheckboxModule, IgxAvatarModule, IgxSelectModule, IgxRadioModule, IgxToastModule
 } from "igniteui-angular";
import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

const MODULES = [

  CommonModule,
  // RouterModule,
  IgxButtonModule,
  IgxIconModule,
  IgxNavigationDrawerModule,
  IgxRippleModule,
  IgxToggleModule,
  IgxInputGroupModule,
  ReactiveFormsModule,
  IgxButtonModule,
  IgxCardModule,
	IgxDividerModule,
	IgxRippleModule,
	IgxChipsModule,
	IgxSliderModule,
	IgxListModule,
  IgxExpansionPanelModule,
  IgxDialogModule,
  IgxTooltipModule,
  IgxTabsModule,
  IgxCheckboxModule,
  IgxAvatarModule,
  IgxSelectModule,
  IgxRadioModule,
  IgxToastModule,
  
]

@NgModule({
  declarations: [
  ],
  imports: MODULES,
  exports: [
    ...MODULES,
  ],
  providers: [
  ],
})
export class SharedModule {}