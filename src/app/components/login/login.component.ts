import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

declare var particlesJS: any;

@Component({
  selector: 'gez-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  returnUrl: string;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authSrv: AuthenticationService,
  ) {
      // redirect to home if already logged in
    if (this.authSrv.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
    // particlesJS.load('particles-js', 'particles.json', null);
    particlesJS.load('particles-js', 'assets/particles-resolve.json', function() {
      console.log('callback - particles.js config cargada');
    });
    this.loginForm = this.fb.group({
      user: [null, Validators.required],
      pass:  [null, Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  logUser(){
    const user = this.loginForm.get('user').value;
    const pass = this.loginForm.get('pass').value;
    // console.log("ogin in", user, pass)
    
  }
}
