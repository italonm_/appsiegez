export class Usuario {
    //id: number;
    codigoUsuario: string;
    codigoLicencia?: string;
    nombre?: string;
    clave: string;
    firstName: string;
    //lastName: string;
    token?: string;
    correo?: string;
    telefono?: string;
    fechaIngreso?: Date;
    usuarioDb?: string;
    sexo?: string;
    foto?: string;
    estado?: string;
    usuarioCreacion?: string;
    fechaCreacion?: Date;
    usuarioModificacion?: string;
    fechaModificacion?: Date;
}
