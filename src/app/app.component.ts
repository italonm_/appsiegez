import { Component } from '@angular/core';

@Component({
  selector: 'gez-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'appSIEGEZ';
}
